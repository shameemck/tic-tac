import 'package:flutter/material.dart';
import 'package:tic_tac_toe/game.dart';

class Users extends StatefulWidget {
  const Users({super.key});

  @override
  State<Users> createState() => _UsersState();
}

class _UsersState extends State<Users> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController player1Ct = TextEditingController();
  TextEditingController player2Ct = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(15),
          children: [
            Text(
              'player details'.toUpperCase(),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: player1Ct,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter name';
                }
                return null;
              },
              decoration: InputDecoration(
                label: Text('Player 1'),
                border: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.circular(10.0), // Set border radius
                  borderSide: BorderSide(
                      color: Colors.black,
                      width: 2.0), // Set border color and width
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              controller: player2Ct,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter name';
                }
                return null;
              },
              decoration: InputDecoration(
                label: Text('Player 2'),
                border: OutlineInputBorder(
                  borderRadius:
                      BorderRadius.circular(10.0), // Set border radius
                  borderSide: BorderSide(
                      color: Colors.black,
                      width: 2.0), // Set border color and width
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) {
                        return GameScreen(
                            player1: player1Ct.text, player2: player2Ct.text);
                      },
                    ));
                  }
                },
                child: Text(
                  'SUBMIT',
                  style: TextStyle(color: Colors.black),
                ))
          ],
        ),
      ),
    );
  }
}
