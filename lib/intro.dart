import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:tic_tac_toe/game.dart';
import 'package:tic_tac_toe/userDetails.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({super.key});

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: Text("X O X", style: TextStyle(fontSize: 40)),
            ),
            Text(
              'Developed by NIHAL',
              style: TextStyle(fontSize: 15),
            ),
            Expanded(
              flex: 2,
              child: AvatarGlow(
                glowColor: Colors.black,
                endRadius: 140.0,
                duration: const Duration(milliseconds: 2000),
                repeat: true,
                showTwoGlows: true,
                shape: BoxShape.circle,
                startDelay: const Duration(seconds: 1),
                repeatPauseDuration: const Duration(milliseconds: 100),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(style: BorderStyle.none)),
                  child: CircleAvatar(
                    backgroundColor: Colors.grey[100],
                    radius: 90.0,
                    child: Image.asset(
                      'assets/images/tic.png',
                      height: 200,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 100),
              width: MediaQuery.of(context).size.width,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) {
                        return const Users();
                      },
                    ));
                  },
                  // Navigator.pushReplacement(context, MaterialPageRoute(
                  //   builder: (context) {
                  //     return const GameScreen();
                  //   },
                  // ));
                  //   showDialog(
                  //     // barrierDismissible: false,
                  //     context: context,
                  //     builder: (context) {
                  //       return AlertDialog(
                  //         contentPadding: const EdgeInsets.all(50),
                  //         backgroundColor: Colors.white,
                  //         surfaceTintColor: Colors.white,
                  //         title: Text('player Details'),
                  //         content: Column(
                  //           children: [
                  //             TextFormField(
                  //               validator: (value) {
                  //                 if (value!.isEmpty) {
                  //                   return 'Please enter some text';
                  //                 }
                  //                 return null;
                  //               },
                  //               decoration: InputDecoration(
                  //                 label: Text('Player 1'),
                  //                 border: OutlineInputBorder(
                  //                   borderRadius: BorderRadius.circular(
                  //                       10.0), // Set border radius
                  //                   borderSide: BorderSide(
                  //                       color: Colors.black,
                  //                       width:
                  //                           2.0), // Set border color and width
                  //                 ),
                  //               ),
                  //             )
                  //             // Text(
                  //             //   'player 1',
                  //             //   style: TextStyle(
                  //             //       fontSize: 20, color: Colors.black),
                  //             // ),
                  //             // Text(
                  //             //   'player 2',
                  //             //   style: TextStyle(
                  //             //       fontSize: 20, color: Colors.black),
                  //             // ),
                  //           ],
                  //         ),
                  //         actions: [
                  //           InkWell(
                  //               onTap: () {
                  //                 if (_formKey.currentState!.validate()) {
                  //                   Navigator.pushReplacement(context,
                  //                       MaterialPageRoute(
                  //                     builder: (context) {
                  //                       return const GameScreen();
                  //                     },
                  //                   ));
                  //                 }
                  //               },
                  //               child: Container(
                  //                 padding: const EdgeInsets.symmetric(
                  //                     vertical: 10, horizontal: 16),
                  //                 decoration: const BoxDecoration(
                  //                     color: Colors.black),
                  //                 child: Text(
                  //                   "Submit",
                  //                   style: TextStyle(
                  //                       fontSize: 16, color: Colors.white),
                  //                 ),
                  //               ))
                  //         ],
                  //       );
                  //     },
                  //   );
                  // },
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text("PLAY GAME",
                        style:
                            const TextStyle(fontSize: 20, color: Colors.black)),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
